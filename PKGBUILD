# Maintainer: Muflone http://www.muflone.com/contacts/english/

pkgname=4kstogram
pkgver=3.4.3.3630
pkgrel=2
pkgdesc="Download and backup Instagram photos shared by any user"
arch=('x86_64')
url="https://www.4kdownload.com/products/product-stogram"
license=('custom:eula')
makedepends=('chrpath' 'imagemagick')
#install=${pkgname}.install
source=("${pkgname}_${pkgver}_amd64.tar.bz2"::"https://dl.4kdownload.com/app/${pkgname}_${pkgver%.*}_amd64.tar.bz2"
        "https://metainfo.manjariando.com.br/${pkgname}/com.${pkgname}.metainfo.xml"
        "https://metainfo.manjariando.com.br/${pkgname}/${pkgname}.png"
        'fix_symlink_path.patch')
sha256sums=('9222618dc32bba5faef8a7a6ca28b00578ba3fc8fd1453d5f6c9fddf6bf627d5'
            '462948f2a80d8bc1881130f22f83e496ef502ed3b340d829976d666b2ebb1318'
            'fa1b864ec6e985103c644417cbcfd37c01320c12616823a386e7955b726f25ec'
            '8745f84d3fac77023c1182177e80c8c91561c2d21c90f071de5d6f4773ad65c3')

prepare() {
    cd "${pkgname}"
    # Remove insecure RPATH
    chrpath --delete "${pkgname}-bin"

    # Fix symlink path
    patch -p1 -i "${srcdir}/fix_symlink_path.patch"
}

_4kstogram_desktop="[Desktop Entry]
Name=4K Stogram
GenericName=4K Stogram
Comment=View instagram photos
Comment[pt_BR]=Ver fotos do instagram
Exec=4kstogram
Terminal=false
Type=Application
Icon=4kstogram.png
Categories=Graphics;Network;Qt;"

build() {
    cd "${srcdir}"
    echo -e "$_4kstogram_desktop" | tee com.${pkgname}.desktop
}

package() {
    depends=('openssl')

    # Install files
    install -m 755 -d "${pkgdir}/usr/lib"
    cp -r "${pkgname}" "${pkgdir}/usr/lib"
    chown root.root "${pkgdir}/usr/lib/${pkgname}"

    # Install launcher file
    install -m 755 -d "${pkgdir}/usr/bin"
    ln -s "/usr/lib/${pkgname}/${pkgname}.sh" "${pkgdir}/usr/bin/${pkgname}"
    # Install license file
    install -m 755 -d "${pkgdir}/usr/share/licenses/${pkgname}"
    install -m 644 -t "${pkgdir}/usr/share/licenses/${pkgname}" "${pkgname}/doc/eula"
        
    # Appstream
    install -Dm644 "${srcdir}/com.${pkgname}.desktop" "${pkgdir}/usr/share/applications/com.${pkgname}.desktop"
    install -Dm644 "${srcdir}/com.${pkgname}.metainfo.xml" "${pkgdir}/usr/share/metainfo/com.${pkgname}.metainfo.xml"

    for size in 22 24 32 48 64 128; do
        mkdir -p "${pkgdir}/usr/share/icons/hicolor/${size}x${size}/apps"
        convert "${srcdir}/${pkgname}.png" -resize "${size}x${size}" \
            "${pkgdir}/usr/share/icons/hicolor/${size}x${size}/apps/${pkgname}.png"
    done
}
